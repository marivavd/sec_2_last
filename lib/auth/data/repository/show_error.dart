import 'package:flutter/material.dart';

void showError(BuildContext context, String e){
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e)));
}