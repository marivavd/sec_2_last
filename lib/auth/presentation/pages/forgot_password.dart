import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:sec_2_last/auth/data/repository/show_error.dart';
import 'package:sec_2_last/auth/data/repository/supabase.dart';
import 'package:sec_2_last/auth/presentation/pages/otp.dart';
import 'package:sec_2_last/auth/presentation/pages/sign_in_page.dart';
import 'package:sec_2_last/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_last/auth/presentation/widgets/text_field.dart';
import 'package:sec_2_last/home/presentation/pages/home_page.dart';

class Forgot_pass extends StatefulWidget {
  const Forgot_pass({super.key});

  @override
  State<Forgot_pass> createState() => _Forgot_passState();
}

class _Forgot_passState extends State<Forgot_pass> {
  var email_controller = TextEditingController();
  bool button = false;
  void is_valid(){
    setState(() {
      button = email_controller.text.isNotEmpty;
    });

  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
          child:
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 155,),
                Text(
                  "Forgot Password",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(height: 8,),
                Text(
                  'Enter your email address',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                SizedBox(height: 56,),
                Custom_Field(label: "Email Address", hint: "***********@mail.com", controller: email_controller, onchange: (new_text){is_valid();}),
                SizedBox(height: 56,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 342,
                    height: 46,
                    child: FilledButton(
                      style: Theme.of(context).filledButtonTheme.style,
                      onPressed: (button)?()async{
                        send_email(email: email_controller.text,
                            onError: (String e){showError(context, e);},
                            onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: email_controller.text)));});
                      }: null,
                      child: Text(
                        "Send OTP",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 16
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Remember password? Back to ",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_in_Page()));
                      },
                      child: Text(
                        "Sign in",
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                      ),
                    )
                  ],
                ),

              ],
            ),
          )),
    );
  }
}
