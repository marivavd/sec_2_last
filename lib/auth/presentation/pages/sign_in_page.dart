import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:sec_2_last/auth/data/repository/show_error.dart';
import 'package:sec_2_last/auth/data/repository/supabase.dart';
import 'package:sec_2_last/auth/presentation/pages/forgot_password.dart';
import 'package:sec_2_last/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_last/auth/presentation/widgets/text_field.dart';
import 'package:sec_2_last/home/presentation/pages/home_page.dart';

class Sign_in_Page extends StatefulWidget {
  const Sign_in_Page({super.key});

  @override
  State<Sign_in_Page> createState() => _Sign_in_PageState();
}

class _Sign_in_PageState extends State<Sign_in_Page> {
  var email_controller = TextEditingController();
  var password_controller = TextEditingController();
  bool password_obscure = true;
  bool check = false;
  bool button = false;
  void is_valid(){
    setState(() {
      button = email_controller.text.isNotEmpty && password_controller.text.isNotEmpty;
    });

  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child:
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 155,),
            Text(
              "Welcome Back",
              style: Theme.of(context).textTheme.titleLarge,
            ),
            SizedBox(height: 8,),
            Text(
              'Fill in your email and password to continue',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            SizedBox(height: 20,),
            Custom_Field(label: "Email Address", hint: "***********@mail.com", controller: email_controller, onchange: (new_text){is_valid();}),
            SizedBox(height: 24,),
            Custom_Field(label: "Password", hint: "**********", controller: password_controller, onchange: (new_text){is_valid();},
              is_obscure: password_obscure, tap_suffix: (){
                setState(() {
                  password_obscure = !password_obscure;
                });
              },),
            SizedBox(height: 17,),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: Row(
                      children: [
                        SizedBox(width: 1,),
                        SizedBox(
                          height: 14,
                          width: 14,
                          child: Checkbox(
                            value: check,
                            side: BorderSide(
                                color: Color(0xFFA7A7A7),
                                width: 1
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)
                            ),
                            activeColor: Color(0xFF006CEC),
                            onChanged: (val){
                              setState(() {
                                check = val!;
                                is_valid();
                              });
                            },
                          ),
                        ),
                        SizedBox(width: 2,),
                        Text(
                          "Remember password",
                          style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12)
                        )
                      ],
                    )),
                InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Forgot_pass()));
                  },
                  child: Text(
                    "Forgot Password?",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12, color: Color(0xFF0560FA))
                  ),
                )
              ],
            ),
            SizedBox(height: 187,),
            Align(
              alignment: Alignment.center,
              child: SizedBox(
                width: 342,
                height: 46,
                child: FilledButton(
                  style: Theme.of(context).filledButtonTheme.style,
                  onPressed: (button)?()async{
                    await sign_in(email: email_controller.text,
                        password: password_controller.text,
                        onError: (String e){showError(context, e);},
                        onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()));});
                  }: null,
                  child: Text(
                    "Log in",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 16
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Already have an account?",
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                ),
                InkWell(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Sign_up_Page()));
                  },
                  child: Text(
                    "Sign Up",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                  ),
                )
              ],
            ),
            SizedBox(height: 18,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [Column(
                children: [
                  Text(
                    "or sign in using",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  SizedBox(height: 8,),
                  Image.asset("assets/Facebook google, apple.png")
                ],
              )],
            ),
            SizedBox(height: 95,)
          ],
        ),
      )),
    );
  }
}
