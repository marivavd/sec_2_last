import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:sec_2_last/auth/data/repository/show_error.dart';
import 'package:sec_2_last/auth/data/repository/supabase.dart';
import 'package:sec_2_last/auth/presentation/pages/new_password.dart';
import 'package:sec_2_last/auth/presentation/pages/sign_in_page.dart';
import 'package:sec_2_last/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_last/auth/presentation/widgets/text_field.dart';
import 'package:sec_2_last/home/presentation/pages/home_page.dart';

class OTP extends StatefulWidget {
  final String email;
  const OTP({super.key, required this.email});

  @override
  State<OTP> createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  var controller = TextEditingController();
  bool button = false;
  bool is_Error = false;



  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var separator = width / 6 - 32;

    return Scaffold(
      body: SingleChildScrollView(
          child:
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 155,),
                Text(
                  "OTP Verification",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(height: 8,),
                Text(
                  'Enter the 6 digit numbers sent to your email',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                SizedBox(height: 55,),
                Pinput(
                  length: 6,
                  controller: controller,
                  separatorBuilder: (context) => SizedBox(width: separator,),
                  defaultPinTheme: PinTheme(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                          border: Border.all(
                              width: 1,
                              color: Color(0xFFA7A7A7)
                          )
                      )
                  ),
                  focusedPinTheme: PinTheme(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                          border: Border.all(
                              width: 1,
                              color: Color(0xFF0560FA)
                          )
                      )
                  ),
                  submittedPinTheme: (!is_Error)?PinTheme(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                          border: Border.all(
                              width: 1,
                              color: Color(0xFF0560FA)
                          )
                      )
                  ): PinTheme(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.zero,
                          border: Border.all(
                              width: 1,
                              color: Color(0xFFED3A3A)
                          )
                      )
                  ),
                  onChanged: (text){
                    setState(() {
                      is_Error = false;
                      button = text.length == 6;
                    });
                  },
                ),
                SizedBox(height: 30,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "If you didn’t receive code, ",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      child: Text(
                        "resend",
                        style: TextStyle(
                          color: Color(0xFF0560FA),
                          fontWeight: FontWeight.w400,
                          fontSize: 14
                        )
                      ),
                      onTap: ()async{
                        await send_email(email: widget.email,
                            onError: (String e){showError(context, e);},
                            onResponse: (){});
                      },
                    )
                  ],
                ),
                SizedBox(height: 84,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 342,
                    height: 46,
                    child: FilledButton(
                      style: Theme.of(context).filledButtonTheme.style,
                      onPressed: (button)?()async{
                        await verify_otp(email: widget.email,
                            code: controller.text,
                            onError: (String e){showError(context, e);},
                            onResponse: (){Navigator.push(context, MaterialPageRoute(builder: (context) => New_pass()));});
                      }: null,
                      child: Text(
                        "Set New Password",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 16
                        ),
                      ),
                    ),
                  ),
                ),

              ],
            ),
          )),
    );
  }
}