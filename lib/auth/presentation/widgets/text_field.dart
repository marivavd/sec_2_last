import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';


class Custom_Field extends StatelessWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool is_obscure;
  final Function(String?) onchange;
  final Function()? tap_suffix;
  final MaskTextInputFormatter? formatter;

  const Custom_Field({super.key, required this.label, required this.hint, required this.controller, required this.onchange, this.is_obscure = false, this.tap_suffix, this.formatter});

  @override
  Widget build(BuildContext context) {
   return Column(
     crossAxisAlignment: CrossAxisAlignment.start,
     children: [
       Text(
         label,
         style: Theme.of(context).textTheme.titleMedium,
       ),
       SizedBox(height: 8,),
       TextField(

         controller: controller,
         onChanged: onchange,
         inputFormatters: (formatter != null) ?[formatter!]: [],
         obscureText: is_obscure,
         obscuringCharacter: '*',
         decoration: InputDecoration(
           border: OutlineInputBorder(

             borderRadius: BorderRadius.circular(4),
             borderSide: BorderSide(
               width: 1,
               color: Color(0xFFA7A7A7),
             )
           ),

           hintText: hint,
           hintStyle: TextStyle(
             fontSize: 14,
             fontFamily: 'Roboto',
             fontWeight: FontWeight.w500,
             color: Color(0xFFCFCFCF)
           ),
           focusedBorder: OutlineInputBorder(
               borderRadius: BorderRadius.circular(4),
               borderSide: BorderSide(
                 width: 1,
                 color: Colors.red,
           )),
           contentPadding: EdgeInsets.symmetric(horizontal: 14, vertical: 10),
           suffixIcon: (tap_suffix != null) ?
               GestureDetector(
                 child: Image.asset('assets/eye-slash.png'),
                 onTap: tap_suffix,
               ): null
         ),
       )
     ],
   );

}
}