import 'package:flutter/material.dart';
import 'package:sec_2_last/auth/presentation/pages/sign_up_page.dart';
import 'package:sec_2_last/core/theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: lightTheme,
      home: const Sign_up_Page(),
    );
  }
}
